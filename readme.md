# A Google-powered, single-page, web-app for telling you what to do with your day.

To run: Download the repository and open `places_api_app.html` in a capable browser.

## Dependencies
* HTML5 Geolocation
* Enabled JavaScript
* Google Maps JavaScript API V3

## Included Libraries
* jQuery
* Foundation 5

## Developed and tested 
* Safari 8.0.6
* Firefox 41.0.1
* Chrome 45.0.2454.101

Kim Mroz, 2015